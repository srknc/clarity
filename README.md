# Clarity

### Adım 1
`npm i --save @cds/core @cds/city @clr/icons @clr/angular @clr/ui  @webcomponents/webcomponentsjs` ile dependency'leri yükle

### Adım 2
`angular.json` içerisindeki şu path'e `projects.ProjectName.architect.build.options` aşağıdaki dosyaları ekle

```json
"styles": [
  "node_modules/@clr/ui/clr-ui.min.css",
  "node_modules/@clr/icons/clr-icons.min.css",
  "node_modules/@cds/city/css/bundles/default.min.css",
  "node_modules/@cds/core/styles/module.shims.min.css",
  "node_modules/@cds/core/global.min.css",
  "node_modules/modern-normalize/modern-normalize.css",
  "src/styles.scss"
],
"scripts": [
  "node_modules/@webcomponents/webcomponentsjs/custom-elements-es5-adapter.js",
  "node_modules/@webcomponents/webcomponentsjs/webcomponents-bundle.js",
  "node_modules/@clr/icons/clr-icons.min.js"
]
```
### Adım 3
`index.html` dosyası içerindeki body'yi şu şekilde güncelle

```html
<body cds-text="body"></body>
```

### Adım 4

`app.module.ts` dosyasını aşağıdaki şekilde güncelle

```typescript
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CdsModule } from '@cds/angular';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CdsModule,
    ClarityModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```


# clarity

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
